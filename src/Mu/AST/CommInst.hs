{-# LANGUAGE DeriveGeneric #-}

module Mu.AST.CommInst (CommInst (..)) where

import Data.Binary (Binary)
import GHC.Generics (Generic)

data CommInst
    = CiUvmNewStack
    | CiUvmKillStack
    | CiUvmThreadExit
    | CiUvmCurrentStack
    | CiUvmSetThreadlocal
    | CiUvmGetThreadlocal
    | CiUvmTr64IsFp
    | CiUvmTr64IsInt
    | CiUvmTr64IsRef
    | CiUvmTr64FromFp
    | CiUvmTr64FromInt
    | CiUvmTr64FromRef
    | CiUvmTr64ToFp
    | CiUvmTr64ToInt
    | CiUvmTr64ToRef
    | CiUvmTr64ToTag
    | CiUvmFutexWait
    | CiUvmFutexWaitTimeout
    | CiUvmFutexWake
    | CiUvmFutexCmpRequeue
    | CiUvmKillDependency
    | CiUvmNativePin
    | CiUvmNativeUnpin
    | CiUvmNativeGetAddr
    | CiUvmNativeExpose
    | CiUvmNativeUnexpose
    | CiUvmNativeGetCookie
    | CiUvmMetaIdOf
    | CiUvmMetaNameOf
    | CiUvmMetaLoadBundle
    | CiUvmMetaLoadHail
    | CiUvmMetaNewCursor
    | CiUvmMetaNextFrame
    | CiUvmMetaCopyCursor
    | CiUvmMetaCloseCursor
    | CiUvmMetaCurFunc
    | CiUvmMetaCurFuncVer
    | CiUvmMetaCurInst
    | CiUvmMetaDumpKeepalives
    | CiUvmMetaPopFramesTo
    | CiUvmMetaPushFrame
    | CiUvmMetaEnableWatchpoint
    | CiUvmMetaDisableWatchpoint
    | CiUvmMetaSetTrapHandler
    | CiUvmIrbuilderNewIrBuilder
    | CiUvmIrbuilderLoad
    | CiUvmIrbuilderAbort
    | CiUvmIrbuilderGenSym
    | CiUvmIrbuilderNewTypeInt
    | CiUvmIrbuilderNewTypeFloat
    | CiUvmIrbuilderNewTypeDouble
    | CiUvmIrbuilderNewTypeUptr
    | CiUvmIrbuilderNewTypeUfuncptr
    | CiUvmIrbuilderNewTypeStruct
    | CiUvmIrbuilderNewTypeHybrid
    | CiUvmIrbuilderNewTypeArray
    | CiUvmIrbuilderNewTypeVector
    | CiUvmIrbuilderNewTypeVoid
    | CiUvmIrbuilderNewTypeRef
    | CiUvmIrbuilderNewTypeIref
    | CiUvmIrbuilderNewTypeWeakref
    | CiUvmIrbuilderNewTypeFuncref
    | CiUvmIrbuilderNewTypeTagref64
    | CiUvmIrbuilderNewTypeThreadref
    | CiUvmIrbuilderNewTypeStackref
    | CiUvmIrbuilderNewTypeFramecursorref
    | CiUvmIrbuilderNewTypeIrbuilderref
    | CiUvmIrbuilderNewFuncsig
    | CiUvmIrbuilderNewConstInt
    | CiUvmIrbuilderNewConstIntEx
    | CiUvmIrbuilderNewConstFloat
    | CiUvmIrbuilderNewConstDouble
    | CiUvmIrbuilderNewConstNull
    | CiUvmIrbuilderNewConstSeq
    | CiUvmIrbuilderNewConstExtern
    | CiUvmIrbuilderNewGlobalCell
    | CiUvmIrbuilderNewFunc
    | CiUvmIrbuilderNewExpFunc
    | CiUvmIrbuilderNewFuncVer
    | CiUvmIrbuilderNewBb
    | CiUvmIrbuilderNewDestClause
    | CiUvmIrbuilderNewExcClause
    | CiUvmIrbuilderNewKeepaliveClause
    | CiUvmIrbuilderNewCscRetWith
    | CiUvmIrbuilderNewCscKillOld
    | CiUvmIrbuilderNewNscPassValues
    | CiUvmIrbuilderNewNscThrowExc
    | CiUvmIrbuilderNewBinop
    | CiUvmIrbuilderNewCmp
    | CiUvmIrbuilderNewConv
    | CiUvmIrbuilderNewSelect
    | CiUvmIrbuilderNewBranch
    | CiUvmIrbuilderNewBranch2
    | CiUvmIrbuilderNewSwitch
    | CiUvmIrbuilderNewCall
    | CiUvmIrbuilderNewTailcall
    | CiUvmIrbuilderNewRet
    | CiUvmIrbuilderNewThrow
    | CiUvmIrbuilderNewExtractvalue
    | CiUvmIrbuilderNewInsertvalue
    | CiUvmIrbuilderNewExtractelement
    | CiUvmIrbuilderNewInsertelement
    | CiUvmIrbuilderNewShufflevector
    | CiUvmIrbuilderNewNew
    | CiUvmIrbuilderNewNewhybrid
    | CiUvmIrbuilderNewAlloca
    | CiUvmIrbuilderNewAllocahybrid
    | CiUvmIrbuilderNewGetiref
    | CiUvmIrbuilderNewGetfieldiref
    | CiUvmIrbuilderNewGetelemiref
    | CiUvmIrbuilderNewShiftiref
    | CiUvmIrbuilderNewGetvarpartiref
    | CiUvmIrbuilderNewLoad
    | CiUvmIrbuilderNewStore
    | CiUvmIrbuilderNewCmpxchg
    | CiUvmIrbuilderNewAtomicrmw
    | CiUvmIrbuilderNewFence
    | CiUvmIrbuilderNewTrap
    | CiUvmIrbuilderNewWatchpoint
    | CiUvmIrbuilderNewWpbranch
    | CiUvmIrbuilderNewCcall
    | CiUvmIrbuilderNewNewthread
    | CiUvmIrbuilderNewSwapstack
    | CiUvmIrbuilderNewComminst
    deriving (Generic)

instance Binary CommInst

instance Enum CommInst where
    fromEnum CiUvmNewStack = 0x201
    fromEnum CiUvmKillStack = 0x202
    fromEnum CiUvmThreadExit = 0x203
    fromEnum CiUvmCurrentStack = 0x204
    fromEnum CiUvmSetThreadlocal = 0x205
    fromEnum CiUvmGetThreadlocal = 0x206
    fromEnum CiUvmTr64IsFp = 0x211
    fromEnum CiUvmTr64IsInt = 0x212
    fromEnum CiUvmTr64IsRef = 0x213
    fromEnum CiUvmTr64FromFp = 0x214
    fromEnum CiUvmTr64FromInt = 0x215
    fromEnum CiUvmTr64FromRef = 0x216
    fromEnum CiUvmTr64ToFp = 0x217
    fromEnum CiUvmTr64ToInt = 0x218
    fromEnum CiUvmTr64ToRef = 0x219
    fromEnum CiUvmTr64ToTag = 0x21a
    fromEnum CiUvmFutexWait = 0x220
    fromEnum CiUvmFutexWaitTimeout = 0x221
    fromEnum CiUvmFutexWake = 0x222
    fromEnum CiUvmFutexCmpRequeue = 0x223
    fromEnum CiUvmKillDependency = 0x230
    fromEnum CiUvmNativePin = 0x240
    fromEnum CiUvmNativeUnpin = 0x241
    fromEnum CiUvmNativeGetAddr = 0x242
    fromEnum CiUvmNativeExpose = 0x243
    fromEnum CiUvmNativeUnexpose = 0x244
    fromEnum CiUvmNativeGetCookie = 0x245
    fromEnum CiUvmMetaIdOf = 0x250
    fromEnum CiUvmMetaNameOf = 0x251
    fromEnum CiUvmMetaLoadBundle = 0x252
    fromEnum CiUvmMetaLoadHail = 0x253
    fromEnum CiUvmMetaNewCursor = 0x254
    fromEnum CiUvmMetaNextFrame = 0x255
    fromEnum CiUvmMetaCopyCursor = 0x256
    fromEnum CiUvmMetaCloseCursor = 0x257
    fromEnum CiUvmMetaCurFunc = 0x258
    fromEnum CiUvmMetaCurFuncVer = 0x259
    fromEnum CiUvmMetaCurInst = 0x25a
    fromEnum CiUvmMetaDumpKeepalives = 0x25b
    fromEnum CiUvmMetaPopFramesTo = 0x25c
    fromEnum CiUvmMetaPushFrame = 0x25d
    fromEnum CiUvmMetaEnableWatchpoint = 0x25e
    fromEnum CiUvmMetaDisableWatchpoint = 0x25f
    fromEnum CiUvmMetaSetTrapHandler = 0x260
    fromEnum CiUvmIrbuilderNewIrBuilder = 0x270
    fromEnum CiUvmIrbuilderLoad = 0x300
    fromEnum CiUvmIrbuilderAbort = 0x301
    fromEnum CiUvmIrbuilderGenSym = 0x302
    fromEnum CiUvmIrbuilderNewTypeInt = 0x303
    fromEnum CiUvmIrbuilderNewTypeFloat = 0x304
    fromEnum CiUvmIrbuilderNewTypeDouble = 0x305
    fromEnum CiUvmIrbuilderNewTypeUptr = 0x306
    fromEnum CiUvmIrbuilderNewTypeUfuncptr = 0x307
    fromEnum CiUvmIrbuilderNewTypeStruct = 0x308
    fromEnum CiUvmIrbuilderNewTypeHybrid = 0x309
    fromEnum CiUvmIrbuilderNewTypeArray = 0x30a
    fromEnum CiUvmIrbuilderNewTypeVector = 0x30b
    fromEnum CiUvmIrbuilderNewTypeVoid = 0x30c
    fromEnum CiUvmIrbuilderNewTypeRef = 0x30d
    fromEnum CiUvmIrbuilderNewTypeIref = 0x30e
    fromEnum CiUvmIrbuilderNewTypeWeakref = 0x30f
    fromEnum CiUvmIrbuilderNewTypeFuncref = 0x310
    fromEnum CiUvmIrbuilderNewTypeTagref64 = 0x311
    fromEnum CiUvmIrbuilderNewTypeThreadref = 0x312
    fromEnum CiUvmIrbuilderNewTypeStackref = 0x313
    fromEnum CiUvmIrbuilderNewTypeFramecursorref = 0x314
    fromEnum CiUvmIrbuilderNewTypeIrbuilderref = 0x315
    fromEnum CiUvmIrbuilderNewFuncsig = 0x316
    fromEnum CiUvmIrbuilderNewConstInt = 0x317
    fromEnum CiUvmIrbuilderNewConstIntEx = 0x318
    fromEnum CiUvmIrbuilderNewConstFloat = 0x319
    fromEnum CiUvmIrbuilderNewConstDouble = 0x31a
    fromEnum CiUvmIrbuilderNewConstNull = 0x31b
    fromEnum CiUvmIrbuilderNewConstSeq = 0x31c
    fromEnum CiUvmIrbuilderNewConstExtern = 0x31d
    fromEnum CiUvmIrbuilderNewGlobalCell = 0x31e
    fromEnum CiUvmIrbuilderNewFunc = 0x31f
    fromEnum CiUvmIrbuilderNewExpFunc = 0x320
    fromEnum CiUvmIrbuilderNewFuncVer = 0x321
    fromEnum CiUvmIrbuilderNewBb = 0x322
    fromEnum CiUvmIrbuilderNewDestClause = 0x323
    fromEnum CiUvmIrbuilderNewExcClause = 0x324
    fromEnum CiUvmIrbuilderNewKeepaliveClause = 0x325
    fromEnum CiUvmIrbuilderNewCscRetWith = 0x326
    fromEnum CiUvmIrbuilderNewCscKillOld = 0x327
    fromEnum CiUvmIrbuilderNewNscPassValues = 0x328
    fromEnum CiUvmIrbuilderNewNscThrowExc = 0x329
    fromEnum CiUvmIrbuilderNewBinop = 0x32a
    fromEnum CiUvmIrbuilderNewCmp = 0x32b
    fromEnum CiUvmIrbuilderNewConv = 0x32c
    fromEnum CiUvmIrbuilderNewSelect = 0x32d
    fromEnum CiUvmIrbuilderNewBranch = 0x32e
    fromEnum CiUvmIrbuilderNewBranch2 = 0x32f
    fromEnum CiUvmIrbuilderNewSwitch = 0x330
    fromEnum CiUvmIrbuilderNewCall = 0x331
    fromEnum CiUvmIrbuilderNewTailcall = 0x332
    fromEnum CiUvmIrbuilderNewRet = 0x333
    fromEnum CiUvmIrbuilderNewThrow = 0x334
    fromEnum CiUvmIrbuilderNewExtractvalue = 0x335
    fromEnum CiUvmIrbuilderNewInsertvalue = 0x336
    fromEnum CiUvmIrbuilderNewExtractelement = 0x337
    fromEnum CiUvmIrbuilderNewInsertelement = 0x338
    fromEnum CiUvmIrbuilderNewShufflevector = 0x339
    fromEnum CiUvmIrbuilderNewNew = 0x33a
    fromEnum CiUvmIrbuilderNewNewhybrid = 0x33b
    fromEnum CiUvmIrbuilderNewAlloca = 0x33c
    fromEnum CiUvmIrbuilderNewAllocahybrid = 0x33d
    fromEnum CiUvmIrbuilderNewGetiref = 0x33e
    fromEnum CiUvmIrbuilderNewGetfieldiref = 0x33f
    fromEnum CiUvmIrbuilderNewGetelemiref = 0x340
    fromEnum CiUvmIrbuilderNewShiftiref = 0x341
    fromEnum CiUvmIrbuilderNewGetvarpartiref = 0x342
    fromEnum CiUvmIrbuilderNewLoad = 0x343
    fromEnum CiUvmIrbuilderNewStore = 0x344
    fromEnum CiUvmIrbuilderNewCmpxchg = 0x345
    fromEnum CiUvmIrbuilderNewAtomicrmw = 0x346
    fromEnum CiUvmIrbuilderNewFence = 0x347
    fromEnum CiUvmIrbuilderNewTrap = 0x348
    fromEnum CiUvmIrbuilderNewWatchpoint = 0x349
    fromEnum CiUvmIrbuilderNewWpbranch = 0x34a
    fromEnum CiUvmIrbuilderNewCcall = 0x34b
    fromEnum CiUvmIrbuilderNewNewthread = 0x34c
    fromEnum CiUvmIrbuilderNewSwapstack = 0x34d
    fromEnum CiUvmIrbuilderNewComminst = 0x34e
    fromEnum CiUvmNewStack = 0x201
    fromEnum CiUvmKillStack = 0x202
    fromEnum CiUvmThreadExit = 0x203
    fromEnum CiUvmCurrentStack = 0x204
    fromEnum CiUvmSetThreadlocal = 0x205
    fromEnum CiUvmGetThreadlocal = 0x206
    fromEnum CiUvmTr64IsFp = 0x211
    fromEnum CiUvmTr64IsInt = 0x212
    fromEnum CiUvmTr64IsRef = 0x213
    fromEnum CiUvmTr64FromFp = 0x214
    fromEnum CiUvmTr64FromInt = 0x215
    fromEnum CiUvmTr64FromRef = 0x216
    fromEnum CiUvmTr64ToFp = 0x217
    fromEnum CiUvmTr64ToInt = 0x218
    fromEnum CiUvmTr64ToRef = 0x219
    fromEnum CiUvmTr64ToTag = 0x21a
    fromEnum CiUvmFutexWait = 0x220
    fromEnum CiUvmFutexWaitTimeout = 0x221
    fromEnum CiUvmFutexWake = 0x222
    fromEnum CiUvmFutexCmpRequeue = 0x223
    fromEnum CiUvmKillDependency = 0x230
    fromEnum CiUvmNativePin = 0x240
    fromEnum CiUvmNativeUnpin = 0x241
    fromEnum CiUvmNativeGetAddr = 0x242
    fromEnum CiUvmNativeExpose = 0x243
    fromEnum CiUvmNativeUnexpose = 0x244
    fromEnum CiUvmNativeGetCookie = 0x245
    fromEnum CiUvmMetaIdOf = 0x250
    fromEnum CiUvmMetaNameOf = 0x251
    fromEnum CiUvmMetaLoadBundle = 0x252
    fromEnum CiUvmMetaLoadHail = 0x253
    fromEnum CiUvmMetaNewCursor = 0x254
    fromEnum CiUvmMetaNextFrame = 0x255
    fromEnum CiUvmMetaCopyCursor = 0x256
    fromEnum CiUvmMetaCloseCursor = 0x257
    fromEnum CiUvmMetaCurFunc = 0x258
    fromEnum CiUvmMetaCurFuncVer = 0x259
    fromEnum CiUvmMetaCurInst = 0x25a
    fromEnum CiUvmMetaDumpKeepalives = 0x25b
    fromEnum CiUvmMetaPopFramesTo = 0x25c
    fromEnum CiUvmMetaPushFrame = 0x25d
    fromEnum CiUvmMetaEnableWatchpoint = 0x25e
    fromEnum CiUvmMetaDisableWatchpoint = 0x25f
    fromEnum CiUvmMetaSetTrapHandler = 0x260
    fromEnum CiUvmIrbuilderNewIrBuilder = 0x270
    fromEnum CiUvmIrbuilderLoad = 0x300
    fromEnum CiUvmIrbuilderAbort = 0x301
    fromEnum CiUvmIrbuilderGenSym = 0x302
    fromEnum CiUvmIrbuilderNewTypeInt = 0x303
    fromEnum CiUvmIrbuilderNewTypeFloat = 0x304
    fromEnum CiUvmIrbuilderNewTypeDouble = 0x305
    fromEnum CiUvmIrbuilderNewTypeUptr = 0x306
    fromEnum CiUvmIrbuilderNewTypeUfuncptr = 0x307
    fromEnum CiUvmIrbuilderNewTypeStruct = 0x308
    fromEnum CiUvmIrbuilderNewTypeHybrid = 0x309
    fromEnum CiUvmIrbuilderNewTypeArray = 0x30a
    fromEnum CiUvmIrbuilderNewTypeVector = 0x30b
    fromEnum CiUvmIrbuilderNewTypeVoid = 0x30c
    fromEnum CiUvmIrbuilderNewTypeRef = 0x30d
    fromEnum CiUvmIrbuilderNewTypeIref = 0x30e
    fromEnum CiUvmIrbuilderNewTypeWeakref = 0x30f
    fromEnum CiUvmIrbuilderNewTypeFuncref = 0x310
    fromEnum CiUvmIrbuilderNewTypeTagref64 = 0x311
    fromEnum CiUvmIrbuilderNewTypeThreadref = 0x312
    fromEnum CiUvmIrbuilderNewTypeStackref = 0x313
    fromEnum CiUvmIrbuilderNewTypeFramecursorref = 0x314
    fromEnum CiUvmIrbuilderNewTypeIrbuilderref = 0x315
    fromEnum CiUvmIrbuilderNewFuncsig = 0x316
    fromEnum CiUvmIrbuilderNewConstInt = 0x317
    fromEnum CiUvmIrbuilderNewConstIntEx = 0x318
    fromEnum CiUvmIrbuilderNewConstFloat = 0x319
    fromEnum CiUvmIrbuilderNewConstDouble = 0x31a
    fromEnum CiUvmIrbuilderNewConstNull = 0x31b
    fromEnum CiUvmIrbuilderNewConstSeq = 0x31c
    fromEnum CiUvmIrbuilderNewConstExtern = 0x31d
    fromEnum CiUvmIrbuilderNewGlobalCell = 0x31e
    fromEnum CiUvmIrbuilderNewFunc = 0x31f
    fromEnum CiUvmIrbuilderNewExpFunc = 0x320
    fromEnum CiUvmIrbuilderNewFuncVer = 0x321
    fromEnum CiUvmIrbuilderNewBb = 0x322
    fromEnum CiUvmIrbuilderNewDestClause = 0x323
    fromEnum CiUvmIrbuilderNewExcClause = 0x324
    fromEnum CiUvmIrbuilderNewKeepaliveClause = 0x325
    fromEnum CiUvmIrbuilderNewCscRetWith = 0x326
    fromEnum CiUvmIrbuilderNewCscKillOld = 0x327
    fromEnum CiUvmIrbuilderNewNscPassValues = 0x328
    fromEnum CiUvmIrbuilderNewNscThrowExc = 0x329
    fromEnum CiUvmIrbuilderNewBinop = 0x32a
    fromEnum CiUvmIrbuilderNewCmp = 0x32b
    fromEnum CiUvmIrbuilderNewConv = 0x32c
    fromEnum CiUvmIrbuilderNewSelect = 0x32d
    fromEnum CiUvmIrbuilderNewBranch = 0x32e
    fromEnum CiUvmIrbuilderNewBranch2 = 0x32f
    fromEnum CiUvmIrbuilderNewSwitch = 0x330
    fromEnum CiUvmIrbuilderNewCall = 0x331
    fromEnum CiUvmIrbuilderNewTailcall = 0x332
    fromEnum CiUvmIrbuilderNewRet = 0x333
    fromEnum CiUvmIrbuilderNewThrow = 0x334
    fromEnum CiUvmIrbuilderNewExtractvalue = 0x335
    fromEnum CiUvmIrbuilderNewInsertvalue = 0x336
    fromEnum CiUvmIrbuilderNewExtractelement = 0x337
    fromEnum CiUvmIrbuilderNewInsertelement = 0x338
    fromEnum CiUvmIrbuilderNewShufflevector = 0x339
    fromEnum CiUvmIrbuilderNewNew = 0x33a
    fromEnum CiUvmIrbuilderNewNewhybrid = 0x33b
    fromEnum CiUvmIrbuilderNewAlloca = 0x33c
    fromEnum CiUvmIrbuilderNewAllocahybrid = 0x33d
    fromEnum CiUvmIrbuilderNewGetiref = 0x33e
    fromEnum CiUvmIrbuilderNewGetfieldiref = 0x33f
    fromEnum CiUvmIrbuilderNewGetelemiref = 0x340
    fromEnum CiUvmIrbuilderNewShiftiref = 0x341
    fromEnum CiUvmIrbuilderNewGetvarpartiref = 0x342
    fromEnum CiUvmIrbuilderNewLoad = 0x343
    fromEnum CiUvmIrbuilderNewStore = 0x344
    fromEnum CiUvmIrbuilderNewCmpxchg = 0x345
    fromEnum CiUvmIrbuilderNewAtomicrmw = 0x346
    fromEnum CiUvmIrbuilderNewFence = 0x347
    fromEnum CiUvmIrbuilderNewTrap = 0x348
    fromEnum CiUvmIrbuilderNewWatchpoint = 0x349
    fromEnum CiUvmIrbuilderNewWpbranch = 0x34a
    fromEnum CiUvmIrbuilderNewCcall = 0x34b
    fromEnum CiUvmIrbuilderNewNewthread = 0x34c
    fromEnum CiUvmIrbuilderNewSwapstack = 0x34d
    fromEnum CiUvmIrbuilderNewComminst = 0x34e
