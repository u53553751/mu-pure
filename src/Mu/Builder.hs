-- |
-- Module      :  Mu.Builder
-- Copyright   :  nathyong 2016
--
-- Maintainer  :  nathyong@gmail.com
-- Stability   :  experimental
-- Portability :  unknown
--
-- Functions for building up Mu programs.
--

{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

module Mu.Builder
    ( -- * MuBuilder Typeclass
      MuBuilder (..)

      -- * Data Structures
    , BuilderState
    , Block
    , Function
    , MuException (..)

      -- * Auxiliary functions
    , flatten
    , emptyBuilderState

      -- * Builder interface
    , getTypeDef
    , getTypeDefs
    , containsType
    , getVarID
    , getConstant
    , getConstants
    , containsConst
    , getFuncSig
    , containsFuncSig
    , getFuncDecl
    , containsFuncDecl
    , getGlobal
    , containsGlobal
    , getFuncDef
    , containsFuncDef
    , putFuncSig
    , putFunction
    , putTypeDef
    , putGlobal
    , putExpose
    , putConstant
    , putFuncDecl
    , createVariable
    , createVariables
    , createExecClause
--     , putBasicBlock
--     , withBasicBlock
--     , updateBasicBlock
--     , putParams
--     , putBinOp
--     , putConvOp
--     , putAtomicRMW
--     , putFence
--     , putNew
--     , putNewHybrid
--     , putAlloca
--     , putAllocaHybrid
--     , setTermInstRet
--     , setTermInstThrow
--     , putCall
--     , putCCall
--     , setTermInstTailCall
--     , setTermInstBranch
--     , setTermInstBranch2
--     , putWatchPoint
--     , setTermInstWatchPoint
--     , putTrap
--     , setTermInstTrap
--     , setTermInstWPBranch
--     , setTermInstSwitch
--     , setTermInstSwapStack
--     , putComminst
--     , putLoad
--     , putStore
--     , putExtractValueS
--     , putInsertValueS
--     , putExtractValue
--     , putInsertValue
--     , putShuffleVector
--     , putGetIRef
--     , putGetFieldIRef
--     , putGetElemIRef
--     , putShiftIRef
--     , putGetVarPartIRef
--     , putComment
--     , putIf
--     , putIfElse
--     , putIfTrue
--     , putWhile
    ) where

import Control.Monad.Except (MonadError, throwError)
import Control.Monad.State.Strict
       (MonadState, State, get, put, gets, modify, evalState)
import Control.Monad.Writer.Strict (WriterT, runWriterT, tell)
import Data.Typeable (Typeable)
import Data.Either (partitionEithers)
import Prelude hiding (EQ)
import qualified Data.Map.Strict as M

import Text.Printf (printf)

import Mu.Syntax
import Mu.PrettyPrint (PrettyPrint(..))


-------------------------------------------------- * MuBuilder class

{-| The 'MuBuilder' class provides the API in a 'bring your own monad' fashion,
    whereby the only requirement is that you provide some means for throwing
    exceptions that may arise as a result of badly specified code generation,
    and you interface with the 'BuilderState' that keeps track of the internals
    of the Mu code.
-}
class MonadError MuException m => MuBuilder m where
    getBuilderState :: m BuilderState
    putBuilderState :: BuilderState -> m ()
    getsBuilderState :: (BuilderState -> a) -> m a
    modifyBuilderState :: (BuilderState -> BuilderState) -> m ()


-------------------------------------------------- * Data types

-- | Contains all the information needed to build a Mu bundle, or program.
data BuilderState = BuilderState
    { builderVarID :: !Int
    , constants :: M.Map String Definition -- ^ Holds all constants, Indexed by constant name
    , typedefs :: M.Map String Definition -- ^ Holds all typedefs, Indexed by type alias
    , funcsigs :: M.Map String Definition -- ^ Holds all function signatures, indexed by function name ++ version
    , funcdecls :: M.Map String Definition -- ^ Holds all function declarations, Indexed by function name ++ version
    , globals :: M.Map String Definition -- ^ Holds all globals, Indexed by variable name
    , exposes :: M.Map String Definition -- ^ Holds all exposes, Indexed by name
    , functionDefs :: M.Map String Definition -- ^ Holds all Function Defs, Indexed by function name ++ version
    }

instance PrettyPrint BuilderState where
    ppFormat = ppFormat . flatten

data Function =
    Function String  -- ^ Function name
             String  -- ^ Function version tag

data Block =
    Block String    -- ^ Block name
          Function  -- ^ Parent function

-- | An exception type for handling Mu errors.
data MuException
    = NotFoundError String
      -- ^ Could not find a corresponding entity for a given name.
    | TypeMismatchError String
      -- ^ Found an entity with the same name, but the wrong type.
    deriving (Typeable, Show)

instance PrettyPrint (Either MuException BuilderState) where
    ppFormat e =
        case e of
            Left err -> return $ show err
            Right bs -> ppFormat bs

instance PrettyPrint (Either MuException Program) where
    ppFormat e =
        case e of
            Left err -> return $ show err
            Right prog -> ppFormat prog


-------------------------------------------------- * The functions

-- | Transform a 'BuilderState' into a 'Program'.
flatten :: BuilderState -> Program
flatten (BuilderState _ cons tds fs fdecl gl ex fdefs) =
    Program $
    concat
        [ M.elems tds
        , M.elems gl
        , M.elems cons
        , M.elems fdecl
        , M.elems fs
        , M.elems ex
        , M.elems fdefs
        ]

emptyBuilderState :: BuilderState
emptyBuilderState =
    BuilderState 0 M.empty M.empty M.empty M.empty M.empty M.empty M.empty

getVarID :: MuBuilder m => m Int
getVarID = getsBuilderState builderVarID
{-# INLINABLE getVarID #-}

getTypeDef :: MuBuilder m => String -> m TypeDef
getTypeDef name = do
    pState <- getsBuilderState typedefs
    case M.lookup name pState of
        Nothing -> throwError (NotFoundError name)
        Just (TypeDef tDef) -> return tDef
        _ -> throwError (TypeMismatchError name)
{-# INLINABLE getTypeDef #-}

getTypeDefs :: MuBuilder m => [String] -> m [TypeDef]
getTypeDefs = mapM getTypeDef
{-# INLINABLE getTypeDefs #-}

containsType :: MuBuilder m => String -> m Bool
containsType name = do
    pState <- getsBuilderState typedefs
    case M.lookup name pState of
        Nothing -> return False
        Just (TypeDef _) -> return True
        Just _ -> throwError (TypeMismatchError name)
{-# INLINABLE containsType #-}

getConstant :: MuBuilder m => String -> m SSAVariable
getConstant name = do
    pState <- getsBuilderState constants
    case M.lookup name pState of
        Nothing -> throwError (NotFoundError name)
        Just (ConstDecl var _) -> return var
        _ -> throwError (TypeMismatchError name)
{-# INLINABLE getConstant #-}

getConstants :: MuBuilder m => [String] -> m [SSAVariable]
getConstants = mapM getConstant
{-# INLINABLE getConstants #-}

containsConst :: MuBuilder m => String -> m Bool
containsConst name = do
    pState <- getsBuilderState constants
    case M.lookup name pState of
        Nothing -> return False
        Just (ConstDecl _ _) -> return True
        Just _ -> throwError (TypeMismatchError name)
{-# INLINABLE containsConst #-}

getFuncSig :: MuBuilder m => String -> m FuncSig
getFuncSig name = do
    pState <- getsBuilderState funcsigs
    case M.lookup name pState of
        Nothing -> throwError (NotFoundError name)
        Just (FunctionSignature sig) -> return sig
        _ -> throwError (TypeMismatchError name)
{-# INLINABLE getFuncSig #-}

containsFuncSig :: MuBuilder m => String -> m Bool
containsFuncSig name = do
    pState <- getsBuilderState funcsigs
    case M.lookup name pState of
        Nothing -> return False
        Just (FunctionSignature _) -> return True
        Just _ -> throwError (TypeMismatchError name)
{-# INLINABLE containsFuncSig #-}

getGlobal :: MuBuilder m => String -> m SSAVariable
getGlobal name = do
    pState <- getsBuilderState globals
    case M.lookup name pState of
        Nothing -> throwError (NotFoundError name)
        Just (GlobalDef var _) -> return var
        _ -> throwError (TypeMismatchError name)
{-# INLINABLE getGlobal #-}

containsGlobal :: MuBuilder m => String -> m Bool
containsGlobal name = do
    pState <- getsBuilderState globals
    return $ case M.lookup name pState of
        Just (GlobalDef _ _) -> True
        _ -> False
{-# INLINABLE containsGlobal #-}

getFuncDecl :: MuBuilder m => String -> m FuncSig
getFuncDecl name = do
    pState <- getsBuilderState funcdecls
    case M.lookup name pState of
        Nothing -> throwError (NotFoundError name)
        Just (FunctionDecl _ sig) -> return sig
        Just _ -> throwError (TypeMismatchError name)
{-# INLINABLE getFuncDecl #-}

containsFuncDecl :: MuBuilder m => String -> m Bool
containsFuncDecl name = do
    pState <- getsBuilderState funcdecls
    return $ case M.lookup name pState of
        Just (FunctionDecl _ _) -> True
        _ -> False
{-# INLINABLE containsFuncDecl #-}

getFuncDef :: MuBuilder m => String -> String -> m Definition
getFuncDef name ver = do
    pState <- getsBuilderState functionDefs
    case M.lookup (name ++ ver) pState of
        Nothing -> throwError (NotFoundError name)
        Just func@(FunctionDef _ _ _ _) -> return func
        Just _ -> throwError (TypeMismatchError name)
{-# INLINABLE getFuncDef #-}

containsFuncDef :: MuBuilder m => String -> String -> m Bool
containsFuncDef name ver = do
    pState <- getsBuilderState functionDefs
    return $ case M.lookup (name ++ ver) pState of
        Just (FunctionDef _ _ _ _) -> True
        _ -> False
{-# INLINABLE containsFuncDef #-}

putFuncSig :: MuBuilder m => String -> [TypeDef] -> [TypeDef] -> m FuncSig
putFuncSig name args ret = do
    let functionSig = FuncSig name args ret
    modifyBuilderState
            (\pState ->
                  pState
                  { funcsigs =
                      M.insert
                          name
                          (FunctionSignature functionSig)
                          (funcsigs pState)
                  })
    return functionSig
{-# INLINABLE putFuncSig #-}

putFunction :: MuBuilder m => String -> String -> FuncSig -> m (Function, SSAVariable)
putFunction name ver sig = do
    modifyBuilderState $
        (\pState ->
              pState
              { functionDefs =
                  M.insert
                      (name ++ ver)
                      (FunctionDef name ver sig [])
                      (functionDefs pState)
              })
    fSig <- putTypeDef (show (FuncRef sig)) (FuncRef sig)
    let funcRef = SSAVariable name fSig
    return $ (Function name ver, funcRef)
{-# INLINABLE putFunction #-}

putConstant :: MuBuilder m => String -> TypeDef -> String -> m SSAVariable
putConstant name constType val = do
    let constVar = SSAVariable name constType
    modifyBuilderState
            (\pState ->
                  pState
                  { constants =
                      M.insert name (ConstDecl constVar val) (constants pState)
                  })
    return constVar
{-# INLINABLE putConstant #-}

putTypeDef :: MuBuilder m => String -> UvmType -> m TypeDef
putTypeDef name uvmType = do
    let tDef = TypeDef name uvmType
    modifyBuilderState
            (\pState ->
                  pState
                  { typedefs = M.insert name (TypeDef tDef) (typedefs pState)
                  })
    return tDef
{-# INLINABLE putTypeDef #-}

putFuncDecl :: MuBuilder m => String -> FuncSig -> m ()
putFuncDecl name fSig =
    modifyBuilderState
        (\pState ->
              pState
              { funcdecls =
                  M.insert name (FunctionDecl name fSig) (funcdecls pState)
              })
{-# INLINABLE putFuncDecl #-}

putGlobal :: MuBuilder m => String -> TypeDef -> m (SSAVariable, TypeDef)
putGlobal name dType = do
    let refType' = IRef dType
    varType' <- putTypeDef (show refType') refType'
    let var = SSAVariable name varType'
    modifyBuilderState
            (\pState ->
                  pState
                  { globals =
                      M.insert name (GlobalDef var dType) (globals pState)
                  })
    return (var, varType')
{-# INLINABLE putGlobal #-}

putExpose :: MuBuilder m => String -> String -> CallConvention -> SSAVariable -> m ()
putExpose name funcName cconv cookie = do
    let e = ExposeDef name funcName cconv cookie
    modifyBuilderState
            (\pState ->
                  pState
                  { exposes = M.insert name e (exposes pState)
                  })
{-# INLINABLE putExpose #-}

createVariable :: MuBuilder m => String -> TypeDef -> m SSAVariable
createVariable name typeVal = do
    n <- getVarID
    modifyBuilderState $
        \pState ->
             pState
             { builderVarID = succ n
             }
    return $ SSAVariable (printf "%s%d" name n) typeVal
{-# INLINABLE createVariable #-}

createVariables :: MuBuilder m => TypeDef -> [String] -> m [SSAVariable]
createVariables t lst = mapM (flip createVariable t) lst
{-# INLINABLE createVariables #-}

createExecClause :: Block -> [SSAVariable] -> Block -> [SSAVariable] -> ExceptionClause
createExecClause (Block name1 _) v1 (Block name2 _) v2 =
    ExceptionClause (DestinationClause name1 v1) (DestinationClause name2 v2)
{-# INLINABLE createExecClause #-}

-- putBasicBlock :: String -> Maybe SSAVariable -> Function -> Builder Block
-- putBasicBlock name exec fn@(Function func ver) = do
--     FunctionDef fName fVer fSig fBody <- eptFuncDef func ver
--     let block = BasicBlock name [] exec [] (Return [])
--     modifyBuilderState $
--         (\pState ->
--               pState
--               { functionDefs =
--                   M.insert
--                       (fName ++ fVer)
--                       (FunctionDef fName fVer fSig (block : fBody))
--                       (functionDefs pState)
--               })
--     return $ Block name fn
-- 
-- newtype BlockState =
--     BlockState ([Assign], [SSAVariable], Maybe Expression)
-- 
-- instance Monoid BlockState where
--     mempty = BlockState ([], [], Nothing)
--     mappend (BlockState (b1, p1, t1)) (BlockState (b2, p2, t2)) =
--         case t2 of
--             Nothing -> BlockState (b2 `mappend` b1, p1 `mappend` p2, t1)
--             _ -> BlockState (b2 `mappend` b1, p1 `mappend` p2, t2)

-- withBasicBlock :: String -> Maybe SSAVariable -> Function -> WriterT BlockState Builder a -> Builder (Block, a)
-- withBasicBlock name exec func prog
--                               --let block = BasicBlock name [] exec [] (Return [])
--  = do
--     block <- putBasicBlock name exec func
--     res <- updateBasicBlock block prog
--     return (block, res)
-- 
-- updateBasicBlock :: Block -> WriterT BlockState Builder a -> Builder a
-- updateBasicBlock block@(Block name (Function func ver)) prog = do
--     FunctionDef fName fVer fSig fBody <- getFuncDef func ver
--     bb@(BasicBlock _ params _ body term) <- getBlock block fBody
--     (ctx, BlockState (body', params', Just term')) <-
--         runWriterT $
--         do tell $ BlockState (body, params, Just term)
--            prog
--     let block' =
--             bb
--             { basicBlockInstructions = body'
--             , basicBlockTerminst = term'
--             , basicBlockParams = params'
--             }
--     newBody <- editBlock block' fBody
--     modifyBuilderState
--             (\pState ->
--                   pState
--                   { functionDefs =
--                       M.insert
--                           (fName ++ fVer)
--                           (FunctionDef fName fVer fSig newBody)
--                           (functionDefs pState)
--                   })
--     return ctx
--   where
--     editBlock :: BasicBlock -> [BasicBlock] -> Builder [BasicBlock]
--     editBlock blk lst =
--         case lst of
--             x:xs
--                 | basicBlockName x == name -> return $ blk : xs
--                 | otherwise -> (x :) <$> (editBlock blk xs)
--             [] -> throwError $ printf "could not find block %s" name
--     getBlock :: Block -> [BasicBlock] -> Builder BasicBlock
--     getBlock blk lst =
--         case lst of
--             x:xs
--                 | name == basicBlockName x -> return x
--                 | otherwise -> getBlock blk xs
--             [] -> throwError $ printf "could not find block %s" name
-- 
-- putParams :: [TypeDef] -> WriterT BlockState Builder [SSAVariable]
-- putParams types = do
--     let vars = genVars 0 types
--     tell $ BlockState ([], vars, Nothing)
--     return vars
--   where
--     genVars :: Int -> [TypeDef] -> [SSAVariable]
--     genVars count lst =
--         case lst of
--             t:ts ->
--                 SSAVariable (printf "p%d" count) t :
--                 genVars (succ count) ts
--             [] -> []
-- 
-- putBinOp :: BinaryOp -> SSAVariable -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putBinOp op v1@(SSAVariable _ opType) v2 exec = do
--     assignee <- lift $ createVariable "v" opType
--     tell $
--         BlockState
--             ([Assign [assignee] (BinaryOperation op opType v1 v2 exec)], [], Nothing)
--     return assignee
-- 
-- putConvOp :: ConvertOp -> TypeDef -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putConvOp op dest var exec = do
--     assignee <- lift $ createVariable "v" dest
--     tell $
--         BlockState
--             ( [ Assign
--                     [assignee]
--                     (ConvertOperation op (varType var) dest var exec)
--               ]
--             , []
--             , Nothing)
--     return assignee
-- 
-- putAtomicRMW :: AtomicRMWOp -> Bool -> MemoryOrder -> SSAVariable -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putAtomicRMW op ptr memOrd loc opnd exec = do
--     assignee <- lift $ createVariable "v" (varType opnd)
--     tell $
--         BlockState
--             ( [ Assign
--                     [assignee]
--                     (AtomicRMWOperation ptr memOrd op (varType opnd) loc opnd exec)
--               ]
--             , []
--             , Nothing)
--     return assignee
-- 
-- putFence :: MemoryOrder -> WriterT BlockState Builder ()
-- putFence memOrd = tell $ BlockState ([Assign [] (Fence memOrd)], [], Nothing)
-- 
-- putNew :: TypeDef -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putNew t exec = do
--     let operation = New t exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
-- 
-- putNewHybrid :: TypeDef -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putNewHybrid t len exec = do
--     let operation = NewHybrid t (varType len) len exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
-- 
-- putAlloca :: TypeDef -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putAlloca t exec = do
--     let operation = Alloca t exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
-- 
-- putAllocaHybrid :: TypeDef -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putAllocaHybrid t len exec = do
--     let operation = AllocaHybrid t (varType len) len exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
-- 
-- setTermInstRet :: [SSAVariable] -> WriterT BlockState Builder ()
-- setTermInstRet rets = tell $ BlockState ([], [], Just $ Return rets)
-- 
-- setTermInstThrow :: SSAVariable -> WriterT BlockState Builder ()
-- setTermInstThrow var = tell $ BlockState ([], [], Just $ Throw var)
-- 
-- putCall :: [SSAVariable] -> SSAVariable -> FuncSig -> [SSAVariable] -> Maybe ExceptionClause -> Maybe [SSAVariable] -> WriterT BlockState Builder ()
-- putCall assignee func sig args exec alive =
--     tell $
--     BlockState
--         ( [Assign assignee (Call sig func args exec (KeepAlive <$> alive))]
--         , []
--         , Nothing)
-- 
-- putCCall :: [SSAVariable] -> CallConvention -> TypeDef -> FuncSig -> SSAVariable -> [SSAVariable] -> Maybe ExceptionClause -> Maybe [SSAVariable] -> WriterT BlockState Builder ()
-- putCCall assignee callConv t sig callee args exec alive =
--     tell $
--     BlockState
--         ( [ Assign
--                 assignee
--                 (CCall callConv t sig callee args exec (KeepAlive <$> alive))
--           ]
--         , []
--         , Nothing)
-- 
-- setTermInstTailCall :: FuncSig
--                     -> SSAVariable
--                     -> [SSAVariable]
--                     -> WriterT BlockState Builder ()
-- setTermInstTailCall sig callee args =
--     tell $ BlockState ([], [], Just $ TailCall sig callee args)
-- 
-- setTermInstBranch :: Block -> [SSAVariable] -> WriterT BlockState Builder ()
-- setTermInstBranch (Block dest _) vars =
--     tell $ BlockState ([], [], Just $ Branch1 $ DestinationClause dest vars)
-- 
-- setTermInstBranch2 :: SSAVariable -> Block -> [SSAVariable] -> Block -> [SSAVariable] -> WriterT BlockState Builder ()
-- setTermInstBranch2 cond (Block trueBlock _) trueVars (Block falseBlock _) falseVars =
--     tell $
--     BlockState
--         ( []
--         , []
--         , Just $
--           Branch2
--               cond
--               (DestinationClause trueBlock trueVars)
--               (DestinationClause falseBlock falseVars))
-- 
-- putWatchPoint :: [SSAVariable] -> SSAVariable -> Int -> [TypeDef] -> BasicBlock -> [SSAVariable] -> BasicBlock -> [SSAVariable] -> Maybe (BasicBlock, [SSAVariable]) -> Maybe [SSAVariable] -> WriterT BlockState Builder ()
-- putWatchPoint assignee name wpid ts (BasicBlock dis _ _ _ _) disArgs (BasicBlock ena _ _ _ _) enaArgs wpexec alive =
--     tell $
--     BlockState
--         ( [ Assign
--                 assignee
--                 (WatchPoint
--                      name
--                      wpid
--                      ts
--                      (DestinationClause dis disArgs)
--                      (DestinationClause ena enaArgs)
--                      wp
--                      (KeepAlive <$> alive))
--           ]
--         , []
--         , Nothing)
--   where
--     wp =
--         case wpexec of
--             Nothing -> Nothing
--             Just (BasicBlock wpBlock _ _ _ _, wpVars) ->
--                 Just $ WPExceptionClause $ DestinationClause wpBlock wpVars
-- 
-- putTrap :: [SSAVariable] -> SSAVariable -> [TypeDef] -> Maybe ExceptionClause -> Maybe [SSAVariable] -> WriterT BlockState Builder ()
-- putTrap assignee name ts exec alive =
--     tell $
--     BlockState
--         ([Assign assignee (Trap name ts exec (KeepAlive <$> alive))], [], Nothing)
-- 
-- setTermInstWatchPoint :: SSAVariable -> Int -> [TypeDef] -> BasicBlock -> [SSAVariable] -> BasicBlock -> [SSAVariable] -> Maybe (BasicBlock, [SSAVariable]) -> Maybe [SSAVariable] -> WriterT BlockState Builder ()
-- setTermInstWatchPoint name wpid ts (BasicBlock dis _ _ _ _) disArgs (BasicBlock ena _ _ _ _) enaArgs wpexec alive =
--     tell $
--     BlockState
--         ( []
--         , []
--         , Just $
--           WatchPoint
--               name
--               wpid
--               ts
--               (DestinationClause dis disArgs)
--               (DestinationClause ena enaArgs)
--               wp
--               (KeepAlive <$> alive))
--   where
--     wp =
--         case wpexec of
--             Nothing -> Nothing
--             Just (BasicBlock wpBlock _ _ _ _, wpVars) ->
--                 Just $ WPExceptionClause $ DestinationClause wpBlock wpVars
-- 
-- setTermInstTrap :: SSAVariable -> [TypeDef] -> Maybe ExceptionClause -> Maybe [SSAVariable] -> WriterT BlockState Builder ()
-- setTermInstTrap name ts exec alive =
--     tell $ BlockState ([], [], Just $ Trap name ts exec (KeepAlive <$> alive))
-- 
-- setTermInstWPBranch :: Int -> BasicBlock -> [SSAVariable] -> BasicBlock -> [SSAVariable] -> WriterT BlockState Builder ()
-- setTermInstWPBranch wpid (BasicBlock disBlock _ _ _ _) disArgs (BasicBlock enaBlock _ _ _ _) enaArgs =
--     tell $
--     BlockState
--         ( []
--         , []
--         , Just $
--           WPBranch
--               wpid
--               (DestinationClause disBlock disArgs)
--               (DestinationClause enaBlock enaArgs))
-- 
-- setTermInstSwitch :: SSAVariable -> BasicBlock -> [SSAVariable] -> [(SSAVariable, BasicBlock, [SSAVariable])] -> WriterT BlockState Builder ()
-- setTermInstSwitch cond (BasicBlock defBlock _ _ _ _) defArgs blocks =
--     tell $
--     BlockState
--         ( []
--         , []
--         , Just $
--           Switch
--               (varType cond)
--               cond
--               (DestinationClause defBlock defArgs)
--               (map toBlocks blocks))
--   where
--     toBlocks :: (SSAVariable, BasicBlock, [SSAVariable])
--              -> (SSAVariable, DestinationClause)
--     toBlocks (condition, (BasicBlock block _ _ _ _), args) =
--         (condition, DestinationClause block args)
-- 
-- setTermInstSwapStack :: SSAVariable -> CurStackClause -> NewStackClause -> Maybe ExceptionClause -> Maybe [SSAVariable] -> WriterT BlockState Builder ()
-- setTermInstSwapStack swappee csClause nsClause exec alive =
--     tell $
--     BlockState $
--     ( []
--     , []
--     , Just $ SwapStack swappee csClause nsClause exec (KeepAlive <$> alive))
-- 
-- putComminst :: [SSAVariable] -> String -> [String] -> [TypeDef] -> [FuncSig] -> [SSAVariable] -> Maybe ExceptionClause -> Maybe [SSAVariable] -> WriterT BlockState Builder ()
-- putComminst assignee name flags types sigs args exec alive =
--     tell $
--     BlockState
--         ( [ Assign
--                 assignee
--                 (Comminst
--                      name
--                      (toMaybe $ map Flag flags)
--                      (toMaybe types)
--                      (toMaybe sigs)
--                      (toMaybe args)
--                      exec
--                      (KeepAlive <$> alive))
--           ]
--         , []
--         , Nothing)
--   where
--     toMaybe :: [a] -> Maybe [a]
--     toMaybe lst =
--         case lst of
--             [] -> Nothing
--             _ -> Just lst
-- 
-- putLoad :: Bool -> Maybe MemoryOrder -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putLoad ptr memOrd var exec = do
--     assignee <- lift $ createVariable "v" vType
--     tell $
--         BlockState
--             ([Assign [assignee] (Load ptr memOrd vType var exec)], [], Nothing)
--     return assignee
--   where
--     vType :: TypeDef
--     vType =
--         case uvmTypeDefType $ varType var of
--             IRef t -> t
--             UPtr t -> t
--             _ -> undefined --varType var --errorful type, but let it fail elsewhere
-- 
-- putStore :: Bool -> Maybe MemoryOrder -> SSAVariable -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder ()
-- putStore ptr memOrd loc newVal exec =
--     tell $
--     BlockState
--         ([Assign [] (Store ptr memOrd locType loc newVal exec)], [], Nothing)
--   where
--     locType :: TypeDef
--     locType =
--         case uvmTypeDefType $ varType loc of
--             IRef t -> t
--             UPtr t -> t
--             _ -> varType loc --errorful type, but let it fail elsewhere
-- 
-- putExtractValueS :: Int
--                  -> SSAVariable
--                  -> Maybe ExceptionClause
--                  -> WriterT BlockState Builder SSAVariable
-- putExtractValueS index opnd exec = do
--     let operation = ExtractValueS (varType opnd) index opnd exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $
--         BlockState
--             ( [Assign [assignee] (ExtractValueS (varType opnd) index opnd exec)]
--             , []
--             , Nothing)
--     return assignee
-- 
-- putInsertValueS :: Int -> SSAVariable -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putInsertValueS index opnd newVal exec = do
--     let operation = InsertValueS (varType opnd) index newVal opnd exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
-- 
-- putExtractValue :: SSAVariable -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putExtractValue opnd index exec = do
--     let operation = ExtractValue (varType opnd) (varType index) opnd index exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
-- 
-- putInsertValue :: SSAVariable -> SSAVariable -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putInsertValue opnd index newVal exec = do
--     let operation =
--             InsertValue (varType opnd) (varType index) opnd index newVal exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
-- 
-- putShuffleVector :: SSAVariable -> SSAVariable -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putShuffleVector v1 v2 mask exec = do
--     let operation = ShuffleVector (varType v1) (varType mask) v1 v2 mask exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
-- 
-- putGetIRef :: SSAVariable
--            -> Maybe ExceptionClause
--            -> WriterT BlockState Builder SSAVariable
-- putGetIRef opnd exec = do
--     let operation = GetIRef opndType opnd exec
--     assT <-
--         let retT = IRef opndType
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
--   where
--     opndType :: TypeDef
--     opndType =
--         case uvmTypeDefType $ varType opnd of
--             Ref t -> t
--             _ -> varType opnd --errorful type, but let it fail elsewhere
-- 
-- putGetFieldIRef :: Bool -> Int -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putGetFieldIRef ptr index opnd exec = do
--     let operation = GetFieldIRef ptr opndType index opnd exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
--   where
--     opndType :: TypeDef
--     opndType =
--         case uvmTypeDefType $ varType opnd of
--             IRef t -> t
--             UPtr t -> t
--             _ -> varType opnd --errorful type, but let it fail elsewhere
-- 
-- putGetElemIRef :: Bool -> SSAVariable -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putGetElemIRef ptr opnd index exec = do
--     let operation = GetElemIRef ptr opndType (varType index) opnd index exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
--   where
--     opndType :: TypeDef
--     opndType =
--         case uvmTypeDefType $ varType opnd of
--             IRef t -> t
--             UPtr t -> t
--             _ -> varType opnd --errorful type, but let it fail elsewhere
-- 
-- putShiftIRef :: Bool -> SSAVariable -> SSAVariable -> Maybe ExceptionClause -> WriterT BlockState Builder SSAVariable
-- putShiftIRef ptr opnd offset exec = do
--     let operation = ShiftIRef ptr opndType (varType offset) opnd offset exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
--   where
--     opndType :: TypeDef
--     opndType =
--         case uvmTypeDefType $ varType opnd of
--             IRef t -> t
--             UPtr t -> t
--             _ -> varType opnd --errorful type, but let it fail elsewhere
-- 
-- putGetVarPartIRef :: Bool
--                   -> SSAVariable
--                   -> Maybe ExceptionClause
--                   -> WriterT BlockState Builder SSAVariable
-- putGetVarPartIRef ptr opnd exec = do
--     let operation = GetVarPartIRef ptr opndType opnd exec
--     assT <-
--         let retT = head $ retType operation
--         in lift $ putTypeDef (show retT) retT
--     assignee <- lift $ createVariable "v" assT
--     tell $ BlockState ([Assign [assignee] operation], [], Nothing)
--     return assignee
--   where
--     opndType :: TypeDef
--     opndType =
--         case uvmTypeDefType $ varType opnd of
--             IRef t -> t
--             UPtr t -> t
--             _ -> varType opnd --errorful type, but let it fail elsewhere
-- 
-- putComment :: String -> WriterT BlockState Builder ()
-- putComment str = tell $ BlockState ([Assign [] (Comment str)], [], Nothing)
-- 
-- type Context = [SSAVariable]
-- 
-- putIf :: Context -> Context -> SSAVariable -> Block -> Function -> (Block -> WriterT BlockState Builder a) -> Builder (Block, Block, a)
-- putIf progCtx contCtx cond entry func prog = do
--     n <- getsBuilderState builderVarID
--     progBlock <- putBasicBlock (printf "progBlock%d" n) Nothing func
--     contBlock <- putBasicBlock (printf "contBlock%d" n) Nothing func
--     modifyBuilderState $
--         \pState ->
--              pState
--              { builderVarID = succ $ builderVarID pState
--              }
--     _ <-
--         updateBasicBlock entry $
--         do setTermInstBranch2 cond progBlock progCtx contBlock contCtx
--     ret <- updateBasicBlock progBlock $ prog contBlock
--     return (progBlock, contBlock, ret)
-- 
-- putIfElse :: Context -> Context -> SSAVariable -> Block -> (Block -> WriterT BlockState Builder a) -> (Block -> WriterT BlockState Builder b) -> Builder (Block, Block, Block, a, b)
-- putIfElse trueCtx falseCtx cond entry@(Block _ func) trueProg falseProg = do
--     n <- getsBuilderState builderVarID
--     trueBlock <- putBasicBlock (printf "trueBlock%d" n) Nothing func
--     falseBlock <- putBasicBlock (printf "falseBlock%d" n) Nothing func
--     contBlock <- putBasicBlock (printf "contBlock%d" n) Nothing func
--     modifyBuilderState $
--         \pState ->
--              pState
--              { builderVarID = succ $ builderVarID pState
--              }
--     _ <-
--         updateBasicBlock entry $
--         do setTermInstBranch2 cond trueBlock trueCtx falseBlock falseCtx
--     tRet <- updateBasicBlock trueBlock $ trueProg contBlock
--     fRet <- updateBasicBlock falseBlock $ falseProg contBlock
--     return (trueBlock, falseBlock, contBlock, tRet, fRet)
-- 
-- putIfTrue :: Context -> Context -> SSAVariable -> Block -> (Block -> WriterT BlockState Builder a) -> Builder (Block, Block, a)
-- putIfTrue progCtx contCtx cond entry@(Block _ func) prog = do
--     n <- getVarID
--     progBlock <- putBasicBlock (printf "progBlock%d" n) Nothing func
--     contBlock <- putBasicBlock (printf "contBlock%d" n) Nothing func
--     modifyBuilderState $
--         \pState ->
--              pState
--              { builderVarID = succ $ builderVarID pState
--              }
--     _ <-
--         updateBasicBlock entry $
--         do setTermInstBranch2 cond progBlock progCtx contBlock contCtx
--     pRet <- updateBasicBlock progBlock $ prog contBlock
--     return (progBlock, contBlock, pRet)
-- 
-- putWhile :: Context -> Block -> (Block -> Block -> WriterT BlockState Builder a) -> (Block -> WriterT BlockState Builder b) -> Builder (Block, Block, Block, a, b)
-- putWhile condCtx entry@(Block _ func) condProg loopProg = do
--     n <- getVarID
--     condBlock <- putBasicBlock (printf "condBlock%d" n) Nothing func
--     loopBlock <- putBasicBlock (printf "loopBlock%d" n) Nothing func
--     contBlock <- putBasicBlock (printf "contBlock%d" n) Nothing func
--     modifyBuilderState $
--         \pState ->
--              pState
--              { builderVarID = succ $ builderVarID pState
--              }
--     _ <- updateBasicBlock entry $ do setTermInstBranch condBlock condCtx
--     cRet <- updateBasicBlock condBlock $ condProg loopBlock contBlock
--     lRet <- updateBasicBlock loopBlock $ loopProg condBlock
--     return (condBlock, loopBlock, contBlock, cRet, lRet)
